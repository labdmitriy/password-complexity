"""Function to generate new features."""

import pandas as pd


def generate_features(features_df: pd.DataFrame) -> pd.DataFrame:
    """Generate new features.

    Args:
        features_df (pd.DataFrame): Original features.

    Returns:
        pd.DataFrame: Original and generated features.
    """
    features_df['password_length'] = features_df['Password'].str.len()
    features_df['unique_chars_count'] = features_df['Password'].apply(lambda x: len(set(x)))
    features_df['unique_chars_prop'] = features_df.eval('unique_chars_count / password_length')
    features_df['is_alnum'] = features_df['Password'].str.isalnum().astype('int')
    features_df['is_alpha'] = features_df['Password'].str.isalpha().astype('int')
    features_df['is_lower'] = features_df['Password'].str.islower().astype('int')
    features_df['is_upper'] = features_df['Password'].str.isupper().astype('int')
    features_df['is_numeric'] = features_df['Password'].str.isnumeric().astype('int')

    features_df = features_df.drop('Password', axis=1)

    return features_df
