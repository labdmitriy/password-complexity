"""Functions for API and UI service implementation."""

import os
from pathlib import Path
from typing import Text, Union

from flask import Flask, redirect, render_template, request, session, url_for
from flask_bootstrap import Bootstrap
from werkzeug.wrappers import Response

from password_complexity.app.forms import PredictionForm
from password_complexity.pipelines.build_features import build_features
from password_complexity.pipelines.make_dataset import make_dataset
from password_complexity.pipelines.predict_model import predict_model
from password_complexity.pipelines.train_model import train_model

CONFIG_PATH = Path(str(os.environ.get('CONFIG_PATH') or 'params.yaml'))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'super secret key'
Bootstrap(app)


@app.route('/create_model')
def create_model() -> Text:
    """Create model by data processing, feature generation and model training.

    Returns:
        Text: Message with model creating results status.
    """
    make_dataset(CONFIG_PATH)
    build_features(CONFIG_PATH)
    train_model(CONFIG_PATH)

    return 'Model is created'


@app.route('/predict')
def predict() -> Text:
    """Predict frequency by password.

    Returns:
        Text: Predicted frequency.
    """
    password = request.args['password']
    prediction = str(predict_model(password, CONFIG_PATH))

    return prediction


@app.route('/', methods=['GET', 'POST'])
def index() -> Union[Response, Text]:
    """Password prediction form processing.

    Returns:
        Text: Form with frequency prediction if password was provided, otherwise - empty form.
    """
    form = PredictionForm()

    if form.validate_on_submit():
        session['password'] = form.password.data
        form.password.data = ''
        prediction = predict_model(session['password'], CONFIG_PATH)
        session['prediction'] = prediction
        return redirect(url_for('index'))

    response = render_template('index.html',
                               form=form,
                               password=session.get('password', ''),
                               prediction=session.get('prediction', ''))
    session['password'] = ''
    session['prediction'] = ''

    return response
