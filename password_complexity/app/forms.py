"""Form definitions."""

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class PredictionForm(FlaskForm):
    """Password frequency prediction form definition.

    Args:
        FlaskForm: Class for inheritance.
    """
    password = StringField('', validators=[DataRequired()])
    submit = SubmitField('Predict')
