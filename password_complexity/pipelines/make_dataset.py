"""Load and process raw data."""

import argparse
from pathlib import Path

import pandas as pd

from password_complexity.data.load import load_csv_data
from password_complexity.data.process import process_dataset
from password_complexity.utils.config import load_config
from password_complexity.utils.logger import get_logger


def make_dataset(config_path: Path) -> None:
    """Load and preprocess raw data and save it.

    Args:
        config_path (Path): Path to config file.
    """
    config = load_config(config_path)
    logger = get_logger('MAKE_DATASET', config.base.log_level)

    logger.info('Load raw train and test datasets')
    train_dataset_path = Path(config.make_dataset.train_dataset)
    test_dataset_path = Path(config.make_dataset.test_dataset)
    train_df = load_csv_data(train_dataset_path)
    test_df = load_csv_data(test_dataset_path)

    logger.info('Process train features and target')
    train_features_df, train_target = process_dataset(train_df, is_train=True)

    logger.info('Process test features')
    test_features_df: pd.DataFrame = process_dataset(test_df, is_train=False)

    logger.info('Save interim datasets')

    train_features_interim = config.make_dataset.train_features_interim
    logger.debug(f'Interim train features path: {train_features_interim}')
    train_features_df.to_feather(train_features_interim)

    train_target_processed = config.make_dataset.train_target_processed
    logger.debug(f'Processed target path: {train_target_processed}')
    train_target.to_feather(train_target_processed)

    test_features_interim = config.make_dataset.test_features_interim
    logger.debug(f'Interim test features path: {test_features_interim}')
    test_features_df.to_feather(test_features_interim)


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()
    config_path = Path(args.config)

    make_dataset(config_path=config_path)
