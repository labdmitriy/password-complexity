"""Functions for model prediction."""

import argparse
from pathlib import Path

import joblib
import numpy as np
import pandas as pd

from password_complexity.features.build import generate_features
from password_complexity.utils.config import load_config
from password_complexity.utils.logger import get_logger


def predict_model(password: str, config_path: Path) -> float:
    """Predict frequency by password.

    Args:
        password (str): Password for prediction.
        config_path (Path): Path to config file.

    Returns:
        float: Predicted frequency for given password.
    """
    config = load_config(config_path)
    logger = get_logger('PREDICT_MODEL', config.base.log_level)

    logger.info('Generate features')
    df = pd.DataFrame({'Password': [password]})
    df = generate_features(df)

    logger.info('Load model')
    model_path = Path(config.train_model.model_path)
    model = joblib.load(model_path)

    logger.info('Make prediction')
    prediction = model.predict(df)[0]
    prediction = np.expm1(prediction)
    logger.info(f'Predicted frequency for password "{password}": {prediction}')

    return prediction


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--password', dest='password', required=True)
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()
    config_path = Path(args.config)
    password = args.password

    predict_model(password=password, config_path=config_path)
