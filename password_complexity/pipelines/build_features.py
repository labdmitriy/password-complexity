"""Build features."""

import argparse
from pathlib import Path

from password_complexity.data.load import load_feather_data
from password_complexity.features.build import generate_features
from password_complexity.utils.config import load_config
from password_complexity.utils.logger import get_logger


def build_features(config_path: Path) -> None:
    """Generate new features and prepare dataset.

    Args:
        config_path (Path): Path to config file.
    """
    config = load_config(config_path)
    logger = get_logger('BUILD_FEATURES', config.base.log_level)

    logger.info('Load interim train/test features')
    train_features_interim = Path(config.make_dataset.train_features_interim)
    test_features_interim = Path(config.make_dataset.test_features_interim)
    train_features_df = load_feather_data(train_features_interim)
    test_features_df = load_feather_data(test_features_interim)

    logger.info('Generate train/test features')
    train_features_df = generate_features(train_features_df)
    test_features_df = generate_features(test_features_df)

    logger.info('Save train/test datasets with generated features')

    train_features_processed = config.build_features.train_features_processed
    logger.debug(f'Processed train features path: {train_features_processed}')
    train_features_df.to_feather(train_features_processed)

    test_features_processed = config.build_features.test_features_processed
    logger.debug(f'Processed test features path: {test_features_processed}')
    test_features_df.to_feather(test_features_processed)


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()
    config_path = Path(args.config)

    build_features(config_path=config_path)
