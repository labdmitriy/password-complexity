"""Train a model."""

import argparse
from pathlib import Path
from typing import Dict

import joblib
from lightgbm import LGBMRegressor
from sklearn.model_selection import GridSearchCV, KFold

from password_complexity.data.load import load_feather_data
from password_complexity.utils.config import load_config
from password_complexity.utils.logger import get_logger


def train_model(config_path: Path) -> None:
    """Train model.

    Args:
        config_path (Path): Path to config file.
    """
    config = load_config(config_path)
    logger = get_logger('TRAIN_MODEL', config.base.log_level)

    train_features_processed = Path(config.build_features.train_features_processed)
    train_target_processed = Path(config.make_dataset.train_target_processed)
    train_features_df = load_feather_data(train_features_processed)
    train_target = load_feather_data(train_target_processed)

    random_state = config.base.random_state
    estimator = LGBMRegressor(random_state=random_state)
    cv = KFold(n_splits=3, shuffle=True, random_state=random_state)
    scoring = 'neg_root_mean_squared_error'
    param_grid: Dict = {}

    logger.info('Train model')
    grid_search = GridSearchCV(estimator, param_grid, scoring=scoring, n_jobs=1, cv=cv, verbose=2)
    grid_search.fit(train_features_df, train_target)
    logger.info(f'Best CV score: {-grid_search.best_score_}')
    logger.info(f'Best parameters: {grid_search.best_params_}')

    logger.info('Save model')
    model_path = Path(config.train_model.model_path)
    joblib.dump(grid_search.best_estimator_, model_path)
    logger.info(f'Model saved to: {model_path}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()
    config_path = Path(args.config)

    train_model(config_path=config_path)
