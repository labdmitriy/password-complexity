"""Functions to process features and target."""
from typing import Tuple, Union

import numpy as np
import pandas as pd


def process_dataset(
        df: pd.DataFrame,
        is_train: bool = True) -> Union[Tuple[pd.DataFrame, pd.DataFrame], pd.DataFrame]:
    """Process features.

    Args:
        df (pd.DataFrame): Raw dataset.
        is_train (bool): Whether dataset is train. Defaults to True.

    Returns:
        pd.DataFrame: Processed dataset.
    """
    features_df = df.copy()

    # Fill null values with string value
    nan_password = 'NAN_PASSWORD'
    features_df['Password'] = df['Password'].fillna(nan_password).astype('str')

    if is_train:
        # Aggregate times for records with duplicated password values
        features_df = features_df.groupby('Password', as_index=False)['Times'].sum()

        target = np.log1p(features_df[['Times']])
        features_df = features_df.drop('Times', axis=1)

        return features_df, target
    else:
        features_df = features_df.drop('Id', axis=1)
        return features_df
