"""Functions to load data."""

from pathlib import Path

import pandas as pd


def load_csv_data(path: Path) -> pd.DataFrame:
    """Load dataset from CSV file.

    Args:
        path (Path): Path to CSV file with dataset.

    Returns:
        pd.DataFrame: Loaded dataset.
    """
    return pd.read_csv(path)


def load_feather_data(path: Path) -> pd.DataFrame:
    """Load dataset from feather file.

    Args:
        path (Path): Path to feather file with dataset.

    Returns:
        pd.DataFrame: Loaded dataset.
    """
    return pd.read_feather(path)
