## Password complexity
### Description
Service to predict password relative frequency based on training data.  
Related course: https://github.com/data-mining-in-action/DMIA_ProductionML_2021_Spring  
Related competition: https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords

### How to run
- Clone repository
- Install Python 3.8.5 and poetry
- In terminal, change directory to the project directory and run:  
`make dirs`  
`make requirements`  
- Add training and test data to `data/raw` folder as train.csv and test.csv files correspondingly
- In terminal, change directory to the project directory and run:  
`export FLASK_APP=password_complexity/app/app.py`  
`flask run --host=0.0.0.0`  

### How to use
Endpoints:
- /create_model  
    - Creating model using dataset processing, features generation and model training. You need to invoke it first to make predictions.  
- /predict?password=<password_value>  
    - Frequency prediction using GET method and query parameter.  
- /  
    - Frequency prediction using web form. 
