FROM python:3.8.5-slim-buster

RUN apt-get update && \
    apt-get install -y curl libgomp1 && \
    useradd -m user

ARG PROJECT_NAME=app

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    POETRY_VERSION=1.1.6 \
    POETRY_HOME="/home/user/poetry" \
    POETRY_VIRTUALENVS_CREATE=true \ 
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    PROJECT_NAME=${PROJECT_NAME} \ 
    PROJECT_PATH="/home/user/${PROJECT_NAME}"
ENV PATH="$POETRY_HOME/bin:$PATH"

RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python && \
    mkdir -p $PROJECT_PATH
WORKDIR $PROJECT_PATH
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

ENV FLASK_APP="password_complexity/app/app.py"

COPY . ./
CMD ["poetry", "run", "flask", "run", "--host=0.0.0.0"]
